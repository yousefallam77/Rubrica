//
//  ViewController.swift
//  Rubrica
//
//  Created by IFTS 01 on 27/01/22.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {

    

    @IBOutlet weak var ContattiLbl: UILabel!
    @IBOutlet weak var ContattiTable: UITableView!
    @IBOutlet weak var ContactSearchBar: UISearchBar!
    
    var contatto: contact?
    var FilterData: [contact]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        ContattiLbl.text = "contatti"
    }
    
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        FilterData = []
//        FilterData = Datastorge.share.dati
//        
//          
//        for i in Datastorge.share.dati[IndexPath.row].Name {
//            if i.uppercased().contains(searchText.uppercased) {
//                FilterData?.append(i)
//                
//            }
//        }
//        //prendre i dati
//        //controllo se è vuota la search bar
//        //se è vuota la papolo con tutti i dati
//        //se non è vuota prendo tutti i dati e verficio uno alla volta
//        //se la ricerca resulta come nei dati allora lo metto nel array
//        //termini i controlli la rimetto nella tabella
//    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Datastorge.share.dati.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! contattiTableViewCell
        cell.NameLbl.text = Datastorge.share.dati[indexPath.row].Name
        return cell 
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        contatto = Datastorge.share.dati[indexPath.row]
        performSegue(withIdentifier: "contdetail", sender: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "contdetail" {
            if let destination = segue.destination as?
                DetailViewController{
                destination.mycont = contatto
            }
        }
    }
}

