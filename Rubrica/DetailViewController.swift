//
//  DetailViewController.swift
//  Rubrica
//
//  Created by IFTS 01 on 27/01/22.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var ImgContact: UIImageView!
    @IBOutlet weak var NameLbl: UILabel!
    @IBOutlet weak var NumberLbl: UILabel!
    @IBOutlet weak var MailLbl: UILabel!
    
    var mycont: contact?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        update()
        
        ImgContact.layer.cornerRadius = 200/2
        
    }

    func update(){
        if let contant = mycont {
            ImgContact.image = mycont?.Img
            NameLbl.text = mycont?.Name
            NumberLbl.text = mycont?.Number
            MailLbl.text = mycont?.mail
        }
    }

}
